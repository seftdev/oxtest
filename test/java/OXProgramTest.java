/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ASUS
 */
public class OXProgramTest {
    
    public OXProgramTest() {
    }

    @Test
    public void testcheckVertical1Win(){
        char table[][] = {{'O', '-', '-'}, 
                          {'O', '-', '-'}, 
                          {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        assertEquals(true,OXProgram.checkVertical(table,currentPlayer,col));
    }
    @Test
    public void testcheckVertical2Win(){
        char table[][] = {{'-', 'O', '-'}, 
                          {'-', 'O', '-'}, 
                          {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        assertEquals(true,OXProgram.checkVertical(table,currentPlayer,col));
    }
    @Test
    public void testcheckVertical3Win(){
        char table[][] = {{'-', '-', 'O'}, 
                          {'-', '-', 'O'}, 
                          {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        assertEquals(true,OXProgram.checkVertical(table,currentPlayer,col));
    }
    
    @Test
    public void testcheckHorizontal1Win(){
        char table[][] = {{'O', 'O', 'O'}, 
                          {'-', '-', '-'}, 
                          {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;
        assertEquals(true,OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testcheckHorizontal2Win(){
        char table[][] = {{'-', '-', '-'}, 
                          {'O', 'O', 'O'}, 
                          {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;
        assertEquals(true,OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testcheckHorizontal3Win(){
        char table[][] = {{'-', '-', '-'}, 
                          {'-', '-', '-'}, 
                          {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 3;
        assertEquals(true,OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testcheckX1Win(){
        char table[][] = {{'O', '-', '-'}, 
                          {'-', 'O', '-'}, 
                          {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true,OXProgram.checkX1(table, currentPlayer));
    }
    
    @Test
    public void testcheckX2Win(){
        char table[][] = {{'-', '-', 'O'}, 
                          {'-', 'O', '-'}, 
                          {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true,OXProgram.checkX2(table, currentPlayer));
    }
}
